<?php

namespace reseed\reWidgets\realert;

use yii\base\Widget;
use reseed\reWidgets\realert\assets\ReAlertAsset;
use yii\web\JsExpression;

class ReAlert extends Widget
{
    public $target = null;
    public $event = null;
    public $alertOptions = [];
    public $beforeAlertCode = null;
    public $afterAlertCode = null;


    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        ReAlertAsset::register($this->view);
        if ($this->target && $this->event) {
            if (!isset($this->alertOptions['message'])) {
                throw new \Exception('ReAlert message not set');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->target && $this->event) {
            $js = '$("' . $this->target . '").' . $this->event . '(function() {';
            if($this->beforeAlertCode) {
                $js .= new JsExpression($this->beforeAlertCode);
            }
            $js .= 'new ReAlert({';
            foreach ($this->alertOptions as $name => $option) {
                $js .= $name . ': "' . $option . '",';
            }
            $js .= '}).show();';
            if($this->afterAlertCode) {
                $js .= new JsExpression($this->afterAlertCode);
            }
            $js .= 'return false;});';
            $this->view->registerJs($js);
        }
    }
}
