## ReAlert widget

Create and display alert box

### Usage:

```
// Load widget
use \reseed\reWidgets\realert\ReAlert;
echo ReAlert::widget();

<script type='text/javascript'>
new ReAlert({
    message: '<strong>Warning!</strong>Hello, world', // Requred, alert message
    alert_type:'warning', // Optional, 'success|info|warning|danger', Default is 'info'
    width : '300px', // Optional, alert window width by pixel, Defalut is 300px
    position : 'top-right', //Optional, 'top-right|top-center|bottom-right', Default is 'top-right'
    target_id : '#alertPosition',  // Optional, Alert window positon, if null, then the alert widnow will be shown on the top-right side.
    css : "custom-class", //Optional, Setting class name to alert box
    autoHideDelay : 7000, //hide after milliseconds, if false, it will stay until user close it. Default is 7000 (7 sec)
}).show();
</script>
```