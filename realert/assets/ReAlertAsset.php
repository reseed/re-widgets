<?php
namespace reseed\reWidgets\realert\assets;

use yii\web\AssetBundle;

/**
 * @author Mark Song <song@reseed-s.com>
 */

class ReAlertAsset extends AssetBundle
{
    public $css = [
    ];

    public $js = [
        'js/ReAlert.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->sourcePath = __DIR__;
        parent::init();
    }
}