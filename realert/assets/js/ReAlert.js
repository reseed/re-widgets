/*
* Generate Alert window library
*
* @author Mark Song <song@reseed-s.com>
*
* How to Use :
*   new ReAlert({
*        message: "<strong>Warning!</strong>Hello, world", // Requred, alert message
*        alert_type:"warning", // Optional, 'success|info|warning|danger', Default is 'info'
*        width : "300px", // Optional, alert window width by pixel
*        position : "top-right", //Optional, 'top-right|top-center|bottom-right', Default is 'top-right'
*        target_id : "#alertPosition",  // Optional, Alert window positon, if null, then the alert widnow will be shown on the top-right side.
*        css : "custom-class", //Optional, Setting class name to alert box
*        autoHideDelay : 7000, //hide after milliseconds, if false, it will stay until user close it. Default is 7000
*    }).show();
*
*/

var ReAlert,
  bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

ReAlert = (function() {
  // target element ID
  ReAlert.prototype.target_id = null;

  // Optional, Alert window type : 'success|info|warning|danger'
  ReAlert.prototype.alert_type = "info";

  // Optional, Alert window position : 'top-right|top-center|bottom-right'
  ReAlert.prototype.position = null;

  // Optional, alert box class name
  ReAlert.prototype.css = "";

  //hide after milliseconds
  ReAlert.prototype.autoHideDelay = 7000;

  // Template
  ReAlert.prototype.tpl = "<div class='alert alert-{TYPE} alert-dismissible reAlertWindow {CSS}' role='alert' style='display:none;{STYLE}'>\
  <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> \
  <div id='alert_message'>{MESSAGE}</div> </div>";

  //Constructor
  function ReAlert(obj) {
    this.show = bind(this.show, this);
    if (typeof obj.message === "undefined") {
      console.log("message field is required");
      return false;
    }else{
      this.tpl = this.tpl.replace("{MESSAGE}", obj.message);
    }

    this.target_id = obj.target_id;

    if (typeof obj.position !== "undefined" && obj.position) {
      this.position = obj.position;
    }

    if (typeof obj.autoHideDelay !== "undefined" && obj.autoHideDelay) {
      this.autoHideDelay = obj.autoHideDelay;
    }

    if (typeof obj.alert_type === "undefined" || !obj.alert_type) {
      this.tpl = this.tpl.replace("{TYPE}", this.alert_type);
    }else{
      this.tpl = this.tpl.replace("{TYPE}", obj.alert_type);
    }

    if (typeof obj.css === "undefined" || !obj.css) {
      this.tpl = this.tpl.replace("{CSS}", this.css);
    }else{
      this.tpl = this.tpl.replace("{CSS}", obj.css);
    }

    if (typeof obj.width === "undefined" || !obj.width) {
      this.tpl = this.tpl.replace("{STYLE}", "width:300px;");
    }else{
      if(!isNaN(obj.width)){
        obj.width += "px";
      }
      this.tpl = this.tpl.replace("{STYLE}", "width:"+obj.width);
    }
  }

  ReAlert.prototype.show = function() {
    if($(".reAlertWindow").length > 5) return false;

    var alertElement = $(this.tpl);

    if (typeof this.target_id === "undefined" || !this.target_id) {
      $("body").append(alertElement);
      // Setting CSS
      switch(this.position) {
        case "bottom-right" :
          alertElement.css({
            "position" : "fixed",
            "right" : "0",
            "bottom" : "0",
            "margin": "10px",
            "z-index" : "99999"});
          break;
        case "top-center" :
          var marginLeft = 0-($(".reAlertWindow").width() / 2);
          alertElement.css({
            "position" : "fixed",
            "margin-top": "10px",
            "left" : "50%",
            "margin-left" : marginLeft,
            "z-index" : "99999"});
          break;
        case "top-right":
        default :
          alertElement.css({
            "position" : "fixed",
            "right" : "0",
            "margin": "10px",
            "z-index" : "99999"});
          break;
      }//end swich
    }else{
      $("#" + this.target_id).append(alertElement);
    }
    this.relocate();
    alertElement.fadeIn();

    var that = this;

    if(this.autoHideDelay !== false) {
      setTimeout(function(){
        alertElement.fadeOut();
        alertElement.remove();
        that.relocate();
      }, 7000);
    }
  },
  ReAlert.prototype.relocate = function(){
    if(typeof(this.target_id) !== undefined && this.target_id){
      return false;
    }
    if(this.position == "bottom-right"){
      var totalOpenedAlertHeight =  20;
      $.each($(".reAlertWindow"), function(idx, e){
        $(e).animate({"bottom" : totalOpenedAlertHeight});
        totalOpenedAlertHeight += ($(e).outerHeight() + 10);
      });
    }else{
      var totalOpenedAlertHeight =  0;
      $.each($(".reAlertWindow"), function(idx, e){
        $(e).animate({"top" : totalOpenedAlertHeight});
        totalOpenedAlertHeight += ($(e).outerHeight() + 10);
      });
    }
  };
  return ReAlert;
})();
