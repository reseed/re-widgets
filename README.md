# Repository for rebecca-app widgets
## Widgets

- ReBox
- Timezone
- ReAlert
- SliderPips

### Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Add

```
{
  "type": "git",
  "url": "git@bitbucket.org:reseed/re-widgets.git"
},
```

to the `repositories` section of your `composer.json` and

```
"reseed/rereca-ui": "*",
```

to the `require` section of your `composer.json` file.
