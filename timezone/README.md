## Timezone

Timezone widget designed to provide selection via select2 with ready timezone data

### Usage:

widget requires 2 parameters: model and attribute

```
<?php
    \reseed\widgets\timezone\Timezone::widget([
        'model' => $model,
        'attribute' => 'timezone'
    ]);
?>
```

## Credits

- [Andrey Andreev](mailto:andreev1024@gmail.com)
- [Domi](mailto:dmitriy@reseed-s.com)
- [Dmitry Fedorov](mailto:klka1@live.ru)
