<?php

namespace reseed\reWidgets\timezone;

use Yii;
use yii\base\Widget;
use kartik\icons\Icon;
use yii\helpers\Html;
use kartik\select2\Select2;
/**
 * Timezone widget designed to provide selection via select2 with ready timezone data
 * Usage: widget requires 2 parameters: model and attribute
 *
 * @author Dmitry Fedorov
 * @package reseed\rereca-ui
 * @since 1.0.6
 */
class Timezone extends Select2
{
    /**
     * The timeZone values array translated with app language
     * @var array
     */
    public $timezones = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $timezones = timezone_identifiers_list();

        foreach ($timezones as $timezone) {
            if ($timezone !== 'UTC') {
                $tempArray = explode('/', $timezone);
                $translatedArray = [];
                foreach ($tempArray as $key => $string) {
                    $translatedArray[$key] = Yii::$app->translate->t($string, 'timezone');
                }
                if (!isset($this->timezones[$translatedArray[0]])) {
                    $this->timezones[$translatedArray[0]] = [];
                }
                $this->timezones[$translatedArray[0]][$tempArray[0] . '/' . $tempArray[1]] = $translatedArray[1];
            } else {
                $this->timezones['UTC'] = [
                    $timezone => Yii::$app->translate->t($timezone, 'timezone')
                ];
            }
        }

        $this->data = $this->timezones;
        $this->size = Select2::SMALL;
        $this->options = [
            'placeholder' => Yii::$app->translate->t('Choose timezone') . ' ...',
        ];

        parent::init();
    }
}
