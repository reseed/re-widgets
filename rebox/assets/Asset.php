<?php
namespace reseed\reWidgets\rebox\assets;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $css = [
        //'style.less',
        'style.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init()
    {
        $this->sourcePath = __DIR__;
        parent::init();
    }
}