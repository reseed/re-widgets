<?php
use yii\helpers\Html;
use reseed\reWidgets\rebox\assets\Asset;

Asset::register($this);
?>

<?php if ($containerOptions): ?>
    <?= Html::beginTag('div', $containerOptions) ?>
<?php endif; ?>

<div class="box">
    <div class="box-header <?= implode(' ', $headerClasses) ?>">
        <?php if ($header): ?>
            <div class="box-name">
                <span><?= $icon; ?></span>
                <?= $title ?>
            </div>
            <!-- button -->
            <?php if ($button): ?>
                <div class="box-icons">
                    <?= $button ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <div class="box-content">
        <?= $content; ?>
    </div>
</div>

<?php if ($containerOptions): ?>
    <?= Html::endTag('div') ?>
<?php endif; ?>

<?php
$this->registerJs('$(".box [data-toggle=\"tooltip\"]").tooltip()');
?>