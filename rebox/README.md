## ReBox

Creates and puts Box on page. Example image:

![picture](rebox_example_image.png)

## Usage:

```
// on your view
<?php 
use reseed\widgets\rebox\ReBox;

$form = ReBox::begin([
    'containerOptions' => [
        'class' => 'containerClass',
    ],
    'header' => [
        'options' => [
            'class' => 'headerClass',
            'title' => 'this is a header title'
        ],
        'icon' => [
            'name' => 'cloud',
            'framework' => 'fa',
            'options' => [],
            'space' => true,
            'tag' => 'i'
        ],
        'button' => [
            'tag' => 'a',
            'text' => Icon::show('pencil'),
            //'url' => yii\helpers\Url::to(['index']),
            'options' => [
                'class'=>'someClass',
                'title' => 'some title',
                'data' => [ // for popup window
                    'toggle' => 'modal',
                    'target' => '#modalLanguage'
                ],
            ]
        ],
    ],
]); ?>

    <h2>this is content</h2>

<?php ReBox::end(); ?>

// ...
```

where  
    
* **containerOptions** are options for Box div-container;
* **header**:
    * **header.options** can contain only two values with keys 'class' and 'title';
    * **header.icon** is array with attributes for [\kartik\icons\Icon::show()](http://demos.krajee.com/icons#icon-show);
    * **header.button**:
        * **header.button.tag** can be 'a' or 'button'. Required;
        * **header.button.text** can be text or icon. Required;
        * **header.button.url** is address;
        * **header.button.options** are options for button or a tag. In example we
         defined data variables so when we click on button, popup window will be 
         appeared. But we can ```//'url' => yii\helpers\Url::to(['index']),``` and on 
         clicking on button, ```index``` page will be opened.

