<?php

namespace reseed\reWidgets\rebox;

use Yii;
use yii\base\Widget;
use kartik\icons\Icon;
use yii\helpers\Html;

/**
 * Class ReBox
 *
 * @package rebox
 */
class ReBox extends Widget
{
    /**
     * The HTML attributes for the div-container.
     * @var boolean|array
     */
    public $containerOptions = false;

    /**
     * Header options
     * @var boolean|array
     */
    public $header = [];

    /**
     * Params for view rendering
     * @var array
     */
    private $params = [];

    public function init()
    {
        parent::init();

        if ($header = $this->header) {
            //  class
            $headerClasses = [];
            if (isset($header['options']['class'])) {
                $headerClasses[] = $header['options']['class'];
            }
            $this->params['headerClasses'] = $headerClasses;

            //  icon
            $icon = '';
            if (isset($header['icon'])) {
                $parametersArr = [];
                $method = (new \ReflectionClass('\kartik\icons\Icon'))->getMethod('show');
                $parameters = $method->getParameters();
                foreach ($parameters as $oneParameter) {
                    if (isset($header['icon'][$oneParameter->getName()])) {
                        $parametersArr[] = $header['icon'][$oneParameter->getName()];
                    } elseif ($oneParameter->isDefaultValueAvailable()) {
                        $parametersArr[] = $oneParameter->getDefaultValue();
                    } else {
                        throw new \Exception('Required attribute was skiped ' . "(icon.{$oneParameter->getName()})");
                    }
                }
                $icon = call_user_func_array ('\kartik\icons\Icon::show', $parametersArr);
            }
            $this->params['icon'] = $icon;

            //  title
            $this->params['title'] = isset($header['options']['title']) ?
                Html::encode($header['options']['title']) : '';

            //  button
            $button = null;
            if (isset($header['button'])) {
                if (! isset($header['button']['tag']) or ! isset($header['button']['text'])) {
                    throw new \Exception('Required attribute was skipped ' . "(button.tag OR button.test)");
                }

                $default = [
                    'options' => [],
                ];
                $header['button'] = array_merge($default, $header['button']);
                $header['button']['tag'] = trim(strtolower($header['button']['tag']));

                switch ($header['button']['tag']) {
                    case 'button':
                        $button = Html::button($header['button']['text'], $header['button']['options']);
                        break;

                    case 'a':
                        if (! isset($header['button']['url'])) {
                            $header['button']['url'] = '#';
                        }
                        $button = Html::a($header['button']['text'], $header['button']['url'], $header['button']['options']);
                        break;
                }
            }
            $this->params['button'] = $button;
        }

        ob_start();
    }

    public function run()
    {
        $content = ob_get_clean();
        return $this->render('index', array_merge([
            'containerOptions' => $this->containerOptions,
            'header' => $this->header,
            'content' => $content,
        ], $this->params));
    }
}
