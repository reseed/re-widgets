<?php

namespace reseed\reWidgets\navigation;

use Yii;
use yii\base\ErrorException;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Navigation widget which will help surfing between single record views.
 *
 * @author iamwebdesigner
 * @since 1.1.0
 */
class Navigation extends Widget
{
	const PREV_TEXT = '<';
	const NEXT_TEXT = '>';
	const EMPTY_URL = '#';
	const BUTTON_CLASS = 'btn btn-primary';
	const LINK_CLASS = 'font-bold';
	/**
	 * An object that implements simply an object with defined public accessible non-static properties.
	 * @var \yii\db\ActiveRecord the data model whose details are to be displayed.
	 */
	public $model;
	/**
	 * Whether to use button or text link. Default true
	 * @var bool
	 */
	public $useButton = true;
	/**
	 * Query parameter name. Default is "id"
	 * @var string
	 */
	public $paramName = 'id';
	/**
	 * Options for the link.
	 * Example:
	 * [
	 *      'class' => 'btn btn-primary'
	 * ]
	 * @var array
	 */
	public $linkOptions = [];
	/**
	 * Additional options for prev link
	 * @var array
	 */
	public $prevOptions = [];
	/**
	 * Additional options for next link
	 * @var array
	 */
	public $nextOptions = [];
	/**
	 * Css class for disabled button
	 * @var string
	 */
	public $disabledPageCssClass = 'disabled';
	/**
	 * Next page link label
	 * @var string
	 */
	public $nextPageLabel = self::NEXT_TEXT;
	/**
	 * Previous page link label
	 * @var string
	 */
	public $prevPageLabel = self::PREV_TEXT;

	/**
	 * Prefix for link css classes for preventing mixes
	 * @var string
	 */
	public static $cssClassPrefix = 're-widgets-nav';
	/**
	 * Process widget properties.
	 * Check property values. If they're empty, set default values for them.
	 */
	public function init()
	{
		parent::init();
		if ($this->model === null || $this->model->getIsNewRecord()) {
			throw new ErrorException($this->translate('Invalid value is assigned for "model" property.'));
		}
	}

	/**
	 * Returns two buttons to navigate between single record views.
	 * @return string
	 */
	public function run()
	{
		$prevUrl = $this->getPrevUrl();
		$nextUrl = $this->getNextUrl();
		$prefix = self::$cssClassPrefix;
		foreach (['prev', 'next'] as $type) {
			$optionField = $type . 'Options';
			$urlVarName = $type . 'Url';
			$this->$optionField = array_merge($this->linkOptions, $this->$optionField);
			$cssClass = $this->useButton ? self::BUTTON_CLASS : self::LINK_CLASS;
			Html::addCssClass($this->$optionField, [$cssClass, $prefix]);
			if ($$urlVarName === self::EMPTY_URL) {
				Html::addCssClass($this->$optionField, [$this->disabledPageCssClass]);
			}
		}
		$css = <<<STYLES
a.$prefix.$this->disabledPageCssClass {
	cursor: not-allowed;
	opacity: 0.65;
	pointer-events: none;
}
a.$prefix:last-child {
	margin-left: 3px;
}
STYLES;
		$this->getView()->registerCss($css);
		return $this->render('index', [
			'widget' => $this,
			'prevUrl' => $prevUrl,
			'nextUrl' => $nextUrl
		]);
	}

	private function translate($message)
	{
		return Yii::$app->translate->t($message);
	}

	/**
	 * Create url to set to links.
	 * @param $sign string more or less sign
	 * @param $order int order ascending or descending
	 * @return string
	 */
	private function getUrl($sign, $order)
	{
		$pkValue = $this->model->primaryKey;
		$pkField = $this->model->primaryKey()[0];
		$data = $this
			->model
			->find()
			->select($pkField)
			->where([$sign, $pkField, $pkValue])
			->orderBy([$pkField => $order])
			->asArray()
			->one();

		if (empty($data)) {
			return self::EMPTY_URL;
		}
		return Url::current([$this->paramName => $data[$pkField]]);
	}

	private function getPrevUrl()
	{
		return $this->getUrl('<', SORT_DESC);
	}


	private function getNextUrl()
	{
		return $this->getUrl('>', SORT_ASC);
	}

}