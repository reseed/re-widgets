<?php
/**
 * @var \common\widgets\NavigationWidget $widget
 * @var string $prevUrl
 * @var string $nextUrl
 */
use yii\helpers\Html;
?>

<style type="text/css">
    a.disabled {
        cursor: not-allowed;
        opacity: 0.65;
        pointer-events: none;
    }
</style>

<?= Html::a($widget->prevPageLabel, $prevUrl, $widget->prevOptions) ?>
<?= Html::a($widget->nextPageLabel, $nextUrl, $widget->nextOptions) ?>