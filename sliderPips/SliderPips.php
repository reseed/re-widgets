<?php

namespace reseed\reWidgets\sliderPips;

use Yii;
use yii\base\Widget;
use kartik\icons\Icon;
use yii\helpers\Html;
use reseed\reWidgets\sliderPips\assets\SliderPipsAsset;

/**
 * SliderPips widget designed to provide creating slider via jQuery-ui-Slider-Pips ( http://simeydotme.github.io/jQuery-ui-Slider-Pips/ )
 * Usage: 
 *  echo SliderPips::widget([
 *     'className' => 'slider',
 *     'sliderOptions' => [
 *         'min' => 1,
 *         'max' => 6
 *     ],
 *     'pipsOptions' => [
 *         'step' => 2
 *     ],
 *     'floatOptions' => [ 
 *         'labels' => [
 *             'Monday',
 *             'Tuesday',
 *             'Wednesday',
 *             'Thursday',
 *             'Friday',
 *             'Saturday'
 *         ]
 *     ],
 *     'onFunctions' => [ 
 *         'slidechange' => 'function(e, ui) {
 *             console.log(e);
 *         }',
 *     ],
 *  ]);
 *
 * @author Doszhan Kalibek <doszhan777@gmail.com>
 * @package reseed\re-widgets
 * @since 1.1.0
 */
class SliderPips extends Widget
{
    /**
     * Name of slider element's class
     * @var string
     */
    public $className = '';

    /**
     * Options of jQuery ui slider in 'key'=>'value' format
     * @var array
     */
    public $sliderOptions = [];

    /**
     * Options of pips in 'key'=>'value' format
     * @var array
     */
    public $pipsOptions = [];

    /**
     * Options of float in 'key'=>'value' format
     * @var array
     */
    public $floatOptions = [];

    /**
     * Functions that will be added to slider by via 'on'
     * @var array
     */
    public $onFunctions = [];
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        SliderPipsAsset::register($this->view);

        $this->registerJsWithOptions();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('index', [
            'sliderClassName' => $this->className
        ]);
    }

    /**
     * Set options
     */
    private function registerJsWithOptions()
    {
        $js = '$(".'.$this->className.'").slider(';
        if ( !empty($this->sliderOptions) ){
            $js .= '{';
            $js .= $this->generateJsCodeByOptionsArray($this->sliderOptions);
            $js .= '}';
        }
        $js .= ')';
        if ( !empty($this->pipsOptions) ){
            $js .= '.slider("pips", {';
            $js .= $this->generateJsCodeByOptionsArray($this->pipsOptions);
            $js .= '})';
        }
        if ( !empty($this->floatOptions) ){
            $js .= '.slider("float", {';
            $js .= $this->generateJsCodeByOptionsArray($this->floatOptions);
            $js .= '})';
        }
        if ( !empty($this->onFunctions) ){
            $js .= $this->generateJsCodeByOnFunctionsArray($this->onFunctions);
        }
        $js .= ';';
        $this->view->registerJs($js);
    }

    private function generateJsCodeByOptionsArray($optionsArray){
        $js = '';
        // $key - name of option, $value - value of option
        foreach ($optionsArray as $key => $value) {
            $js .= $key.': ';
            if ( is_string($value) ){
                $js .= '"'.$value.'", ';
            } else if ( is_bool($value) ){
                $js .= $value ? 'true, ' : 'false, ';
            } else if ( is_array($value) ){
                $js .= '["'.implode('", "', $value).'"]';
            } else {
                $js .= $value.', ';
            }
        }
        return $js;
    }

    private function generateJsCodeByOnFunctionsArray($onFunctions){
        $js = '';
        // $key - name of event, $value - function code
        foreach ($onFunctions as $key => $value) {
            $js .= '.on("'.$key.'", '.$value.')';
        }
        return $js;
    }

}
