## SliderPips

Widget for using [jQuery SliderPips](http://simeydotme.github.io/jQuery-ui-Slider-Pips/)

### Usage:

```
<?php
    echo SliderPips::widget([
        'className' => 'slider',
        'sliderOptions' => [
            'min' => 1,
            'max' => 6
        ],
        'pipsOptions' => [
            'step' => 2
        ],
        'floatOptions' => [ 
            'labels' => [
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ]
        ],
        'onFunctions' => [ 
            'slidechange' => 'function(e, ui) {
                console.log(e);
            }',
        ],
    ]);

?>
```

## Credits

- [Doszhan Kalibek](mailto:doszhan777@gmail.com)