<?php
namespace reseed\reWidgets\sliderPips\assets;

use yii\web\AssetBundle;

/**
 * SliderPipsAsset class represents a collection of asset files, such as CSS, JS, images.
 * When select one option, it'll excute ajax to update role for user.
 * 
 * @author Doszhan Kalibek <doszhan777@gmail.com>
 */

class SliderPipsAsset extends AssetBundle
{
    public $css = [
        'css/jquery-ui-slider-pips.css',
    ];

    public $js = [
        'js/jquery-ui-slider-pips.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

    public function init()
    {
        $this->sourcePath = __DIR__;
        parent::init();
    }
}